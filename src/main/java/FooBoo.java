import java.util.List;
import java.util.stream.IntStream;

public class FooBoo {

    public void run(List<Divider> dividerList, int min, int max ) {
        IntStream.range(min, max + 1)
                .forEach(n -> dividerList
                        .forEach(d -> displayIfDivisible(n, d)));
    }

    private static void displayIfDivisible(int number, Divider divider) {
        if (number % divider.getNumber() == 0) System.out.println(number + ": " + divider.getTextToDisplay());
    }
}
