public class Divider {

    private int number;
    private String textToDisplay;

    public Divider(int number, String textToDisplay) {
        this.number = number;
        this.textToDisplay = textToDisplay;
    }

    public int getNumber() {
        return number;
    }

    public String getTextToDisplay() {
        return textToDisplay;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setTextToDisplay(String textToDisplay) {
        this.textToDisplay = textToDisplay;
    }
}
