import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.AggregateWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

public class FooBooTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final FooBoo fooBoo = new FooBoo();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restore(){
        System.setOut(originalOut);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/FooBooTest-runTest-parameters.csv", delimiterString = ";", lineSeparator = "~~")
    public void runTest(int min,
                        int max,
                        @AggregateWith(DividerArgumentConverter.class) List<Divider> dividerList,
                        @AggregateWith(ExpectedArgumentConventer.class) String expected ) {
        fooBoo.run(dividerList, min, max);
        Assertions.assertEquals(expected.trim(), outContent.toString().trim());
    }
}
