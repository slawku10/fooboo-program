import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.aggregator.ArgumentsAggregationException;
import org.junit.jupiter.params.aggregator.ArgumentsAggregator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DividerArgumentConverter implements ArgumentsAggregator {

    public static final int NUMBER_PREVIOUS_PARAMETER = 2;
    public static final int NUMBER_OF_PARAMETRS_DIVIDER_CONSTRUCTOR = 2;
    public static final int NUMBER_NEXT_PARAMETER = 1;

    @Override
    public Object aggregateArguments(ArgumentsAccessor argumentsAccessor, ParameterContext parameterContext)
            throws ArgumentsAggregationException {
        return buildDivider(argumentsAccessor
                .toList()
                .stream()
                .limit(argumentsAccessor.size() - NUMBER_NEXT_PARAMETER)
                .skip(NUMBER_PREVIOUS_PARAMETER)
                .collect(Collectors.toList()));
    }

    private List<Divider> buildDivider(List<Object> argumentList) {
        if (argumentList.isEmpty()) throw new ArgumentsAggregationException("Must be minimum one divider");
        if (argumentList.size() % 2 != 0) throw new ArgumentsAggregationException("Divider must contain number and text to display");

        List<Divider> dividerList = new ArrayList<>();
        for (int i = 0; i <= argumentList.size() - NUMBER_OF_PARAMETRS_DIVIDER_CONSTRUCTOR;
             i = i + NUMBER_OF_PARAMETRS_DIVIDER_CONSTRUCTOR) {
            dividerList.add(new Divider(
                    Integer.parseInt((String) argumentList.get(i)),
                    (String) argumentList.get(i + 1)));
        }
        return dividerList;
    }
}
